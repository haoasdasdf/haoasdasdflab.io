---
title: 在Debian中安裝docker
tags:
  - debian
  - linux
  - docker
date: 2018-12-07 18:30:31
---


## 前言

我雖然才開始學習與了解這項技術，但其實Docker已經是近幾年相當熱門和成熟的技術了。
由於自己本身也是初學者，若有觀念錯誤或任何建議，真誠的希望各位進行指點。

> 在開發調試的過程中，經常會有需要切換多個版本。若是在同一個host或VM中安裝的話，很麻煩也有可能無法進行。
> 以前人們會使用Virtualbox,VMware之類的軟件，進行創建VM。但VM的創建需要從OS開始安裝，所佔體積及時間都不少。
> 所以後來Docker技術就逐漸進入大家視野，到現在為止，Docker技術可以說是很成熟了。

## 用途

人們常用Docker的輕便特性來創建不同的環境以進行版本切換和防止環境污染。

## 安裝步驟

### 操作環境

- 物理環境:
  - 不管是物理服務器，還是虛擬服務器都可以。(ovz虛擬化除外)
- OS:
  - 不管是Linux還是MacOS，Windows基本上都以支持Docker

### 安裝

本篇所使用的是Docker官方提供的 Docker Install Script

- 更新資源安裝庫和已安裝軟件

```bash
# Debian/ubuntu
apt-get update && apt-get upgrade -y

# CentOS
yum update
```

- 安裝所需軟件

```bash
# Debian/ubuntu
apt-get install -y wget ca-certificates

# CentOS
yum install-y wget ca-certificates
```

- 執行Docker安裝Script

```bash
wget -O- https://get.docker.com | bash
```

- 確認Docker版本

```bash
docker -v
# Docker version 18.09.0, build 4d60db4
```

# 以上就完成了簡單的安裝步驟