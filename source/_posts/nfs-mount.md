---
title: 利用NFS進行遠程掛載
tags:
  - linux
  - debian
date: 2018-10-21 00:43:39
---

## 操作系統

- Debian 9 64bit

---

## 服務器端安裝nfs

1.安裝所需程序

  ```bash
  apt-get update && apt-get install nfs-kernel-server
  ```

2.編輯/etc/exports，添加想要共享的目錄與權限

  ```bash
  # `shared source` `client ip`(`share option1, ... ,share optionN`)
  /home/shared 192.168.1.100(ro,sync,no_root_squash)
  /mnt *(rw,sync,root_squash) # *代表允許所有ip地址
  ```

3.編輯完之後要執行以下命令進行重新讀取

  ```bash
  exportfs -a
  ```

- 選項：

  1) rw --> //讀寫，客戶端擁有讀寫的權限。
  2) ro --> //只讀，客戶端只有讀的權限。
  3) no_root_squash --> //root用戶可訪問，不變成匿名用戶，即root用戶可以以root用戶的權限訪問NFS服務器的共享資源。
  4) root_squash --> //root用戶變成匿名用戶
  5) all_squash --> //所有用戶都變成匿名用戶
  6) anonuid
  7) anongid --> //所有匿名用戶的UID或組ID都變為後面設定的UID或GID的權限。因為所有在NFS服務器用戶列表中沒有用戶名的都會匿名用戶，這裡設定這些匿名用戶的用戶權限。
  8) sync --> //同步到硬盤
  9) async --> //數據存放到內存而不是直接寫到硬盤

> 如果要更改端口需要編輯/etc/default/nfs-kernel-server

  ```bash
  # add --port `port`
  RPCMOUNTDOPTS="--manage-gids" # before (defualt is 2049)
  RPCMOUNTDOPTS="--port 33333 --manage-gids" # after
  ```

4.編輯完需要重啟nfs-kernel-server

  ```bash
  systemctl restart nfs-kernel-server
  # or
  /etc/init.d/nfs-kernel-server restart
  ```

5.如果有防火牆的話記得開啟以下端口，可以用`rpcinfo -p`查看

  ```bash
  root@host:~# rpcinfo -p
     program vers proto port service
      100000 4 tcp 111 portmapper
      100000 3 tcp 111 portmapper
      100000 2 tcp 111 portmapper
      100000 4 udp 111 portmapper
      100000 3 udp 111 portmapper
      100000 2 udp 111 portmapper
      100005 1 udp 33333 mountd
      100005 2 udp 33333 mountd
      100005 3 udp 33333 mountd
      100003 3 tcp 2049 nfs
      100003 4 tcp 2049 nfs
      100227 3 tcp 2049
      100003 3 udp 2049 nfs
      100003 4 udp 2049 nfs
      100227 3 udp 2049
      100021 1 udp 48196 nlockmgr
      100021 3 udp 48196 nlockmgr
      100021 4 udp 48196 nlockmgr
      100021 1 tcp 41019 nlockmgr
      100021 3 tcp 41019 nlockmgr
      100021 4 tcp 41019 nlockmgr
  ```

---

## 客戶端安裝nfs

1.安裝nfs客戶端`nfs-common`

  ```bash
  apt-get install nfs-common
  ```

2.建立掛載文件夾並掛載

  ```bash
  mkdir -p /mnt/shared
  # 掛載 mount -t `protocol` `server ip`:`source dir` `target dir`
  mount -t nfs4 192.168.1.1:/home/shared /mnt/shared
  ```

**如需自動掛載，請修改`/etc/fstab`**

  ```bash
  # 增加以下行
  192.168.1.1:/home/shared /mnt/shared nfs4 defaults,rw 0 0
  ```

---

## 備註

1. 如果無法掛載，請檢查防火牆設置，及文件夾權限。