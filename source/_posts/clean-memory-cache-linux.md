---
title: 如何在linux中手動清理memory cache
tags:
  - linux
  - debian
  - ubuntu
date: 2019-01-29 19:06:38
---

## 前言
> 在使用`free -h`查看內存剩餘是，經常會遇到buff/cache很大，free卻很少情況，這篇文章就來幫大家清理一下內存中的 buff/cache

### 首先我們來看一下`free -h`命令下內存的各個參數

```bash
free -h
              total        used        free      shared  buff/cache   available
Mem:           485M        222M         38M        7.2M        223M        243M
Swap:            0B          0B          0B

```
|name|Description|
|---|---
|total|總物理內存|
|used|已使用內存，一般情況這個值會比較大，因為這個值包括了緩存+應用程序使用的內存|
|free|完全未被使用的內存|
|shared|應用程序共享內存|
|buffers|緩存，主要用於目錄方面，索引節點值等|
|cached|緩存，用於已打開的文件|
|available|總可用內存|

從上表中我們可以看出，`可用內存 = 空閒內存 + 緩存`

其實當你讀寫文件的時候，Linux的內核為了提高讀寫性能與速度，會將文件在內存中進行緩存，這部分內存就是Cache Memory（緩存內存）。即使你的程序運行結束後，Cache Memory也不會自動釋放。

這就會導致你在Linux的系統中程序頻繁讀寫文件後，你會發現可用物理內存會很少。其實這緩存內存（Cache Memory）在你需要使用內存的時候會自動釋放，所以你不必擔心沒有內存可用。

如果你希望手動去釋放Cache Memory也是有辦法的。

### 手動釋放緩存

/proc是一個虛擬文件系統，我們可以通過對它的讀寫操作做為與kernel實體間進行通信的一種手段。也就是說可以通過修改/proc中的文件，來對當前kernel的行為做出調整。那麼我們可以通過調整/proc/sys/vm/drop_caches來釋放內存。操作如下：

```bash
cat /proc/sys/vm/drop_caches
0
```

首先，/proc/sys/vm/drop_caches的值，默认为0。

#### 釋放前要進行同步

手動執行`sync`命令（描述：sync 命令運行sync 子例程。如果必須停止系統，則運行sync 命令以確保文件系統的完整性。sync 命令將所有未寫的系統緩衝區寫到磁盤中，包含已修改的i-node、已延遲的塊I/O 和讀寫映射文件


#### 釋放緩存

##### 只釋放 page cache

```bash
sync;echo 1 > /proc/sys/vm/drop_caches
```

##### 釋放dentry和inodes

```bash
sync;echo 2 > /proc/sys/vm/drop_caches
```

##### 釋放page cache，dentry和inodes

```bash
sync;echo 3 > /proc/sys/vm/drop_caches
```

#### cache說明

##### Page cache

從磁盤讀取過一次的文件數據存儲在此區域中。

##### Dentry

為了加速文件路徑搜索，在名為__dentry__的區域中緩存目錄路徑名稱

##### inodes

將權限和所屬組，創建者，創建日期和時間，大小等信息存儲並緩存在名為__ inodes__的區域中。


**如果必須清除磁盤緩存，使用`echo 1 > ...`在生產環境最安全，因為`echo 1 > ...`只清除PageCache。**

**關於`echo 3 > ...`,在您知道自己在做什麼之前，不建議在生產環境中使用，因為它將清除PageCache，dentries和inode。**