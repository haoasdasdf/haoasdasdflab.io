---
title: 用樹莓派做自己的網盤 - nextcloud
date: 2018-10-20 00:43:40
tags:
  - linux
  - raspi
  - nextcloud
---

>近幾年大陸有很多起網盤照片洩漏的事件，所以很少使用那些公共網盤。  
但手機中的照片或文件越來越多，備份到電腦又覺得麻煩。所以就在網上找了一些自己搭建網盤的方法。
找到比較好的幾個是nextcloud，owncloud，seafile。  
在這三個中比較了一下最終選定了nextcloud。應為他的免費功能最多，可以滿足我全部要求。

**如果覺得麻煩的可以選擇官網去購買搭建好的成品**
[NextcloudPi](https://nextcloudpi.com/)

## 搭建步驟

關於將OS燒入SD card等部分我將會省略。

### 我的環境

#### hardware

- Raspberry Pi 3B
- 8G SD car
- 100GB hard disk
- USB插口硬盤盒
- 
#### 使用的OS

- 我自己使用的為官方的raspbian

**由於最新版的raspbian是32bit，如果想要傳輸如2G，4G大文件的話，需要安裝64bit的OS。如 Fedora，OpenSUSE等。**

#### software

- nextcloud @13.0.6
- nginx @1.15.2
- PHP @7.2.9
- MYSQL @5.6.41

### 下載安裝必要文件

1. 按照慣例要進行

```bash
apt-get update && apt-get upgrade -y
```

2. 安裝nginx，mysql，php

#### 我推薦使用軍哥一鍵安裝，方便之後添加ssl等設置。（可以在以下地址生成無人值守參數）

```bash
https://lnmp.org/auto.html
```

由於安裝可能需要一段時間，為了防止ssh斷開我們需要啟動screen

```bash
apt-get install screen -y && screen -S lnmp
```

我所安裝的為nginx + mysql 5.6 + php 7.2 (數據庫密碼為root)

```bash
wget http://soft.vpser.net/lnmp/lnmp1.5.tar.gz -cO lnmp1.5.tar.gz && tar zxf lnmp1.5.tar.gz && cd lnmp1.5 && LNMP_Auto="y" DBSelect="3" DB_Root_Password="root" InstallInnodb="y" PHPSelect="8" SelectMalloc="1" ./install.sh lnmp
```

安裝完可能不是當時最新版本，所以需要升級的話可以進行upgrade
```bash

path> ./upgrade.sh # 然後輸入版本

# 添加一個vhost並啟用ssl
path> lnmp vhost add
# 記得開啟'php-pathinfo'
```

默認的文件是

```bash
/home/wwwroot/your.domain/
```

#### 從APT庫中安裝的話可能版本較舊如果從APT庫中安裝的話可能版本較舊

```bash
apt-get install nginx-full php7* mysql-server
# 安裝的db為MariaDB，與MySQL基本通用
```

添加一個域名的nginx設置文件

```bash
touch /etc/nginx/conf.d/your.domain.conf
```

然後請參考官網配置文件

```bash
upstream php-handler {
    server 127.0.0.1:9000;
    #server unix:/var/run/php5-fpm.sock;
}

server {
    listen 80;
    listen [::]:80;
    server_name cloud.example.com;
    # enforce https
    return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name cloud.example.com;

    ssl_certificate /etc/ssl/nginx/cloud.example.com.crt;
    ssl_certificate_key /etc/ssl/nginx/cloud.example.com.key;

    # Add headers to serve security related headers
    # Before enabling Strict-Transport-Security headers please read into this
    # topic first.
    # add_header Strict-Transport-Security "max-age=15768000;
    # includeSubDomains; preload;";
    #
    # WARNING: Only add the preload option once you read about
    # the consequences in https://hstspreload.org/. This option
    # will add the domain to a hardcoded list that is shipped
    # in all major browsers and getting removed from this list
    # could take several months.
    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Robots-Tag none;
    add_header X-Download-Options noopen;
    add_header X-Permitted-Cross-Domain-Policies none;

    # Path to the root of your installation
    root /var/www/nextcloud/;

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    # The following 2 rules are only needed for the user_webfinger app.
    # Uncomment it if you're planning to use this app.
    #rewrite ^/.well-known/host-meta /public.php?service=host-meta last;
    #rewrite ^/.well-known/host-meta.json /public.php?service=host-meta-json
    # last;

    location = /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }
    location = /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }

    # set max upload size
    client_max_body_size 512M;
    fastcgi_buffers 64 4K;

    # Enable gzip but do not remove ETag headers
    gzip on;
    gzip_vary on;
    gzip_comp_level 4;
    gzip_min_length 256;
    gzip_proxied expired no-cache no-store private no_last_modified no_etag auth;
    gzip_types application/atom+xml application/javascript application/json application/ld+json application/manifest+json application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;

    # Uncomment if your server is build with the ngx_pagespeed module
    # This module is currently not supported.
    #pagespeed off;

    location / {
        rewrite ^ /index.php$request_uri;
    }

    location ~ ^/(?:build|tests|config|lib|3rdparty|templates|data)/ {
        deny all;
    }
    location ~ ^/(?:\.|autotest|occ|issue|indie|db_|console) {
        deny all;
    }

    location ~ ^/(?:index|remote|public|cron|core/ajax/update|status|ocs/v[12]|updater/.+|ocs-provider/.+)\.php(?:$|/) {
        fastcgi_split_path_info ^(.+?\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        fastcgi_param HTTPS on;
        #Avoid sending the security headers twice
        fastcgi_param modHeadersAvailable true;
        fastcgi_param front_controller_active true;
        fastcgi_pass php-handler;
        fastcgi_intercept_errors on;
        fastcgi_request_buffering off;
    }

    location ~ ^/(?:updater|ocs-provider)(?:$|/) {
        try_files $uri/ =404;
        index index.php;
    }

    # Adding the cache control header for js and css files
    # Make sure it is BELOW the PHP block
    location ~ \.(?:css|js|woff|svg|gif)$ {
        try_files $uri /index.php$request_uri;
        add_header Cache-Control "public, max-age=15778463";
        # Add headers to serve security related headers (It is intended to
        # have those duplicated to the ones above)
        # Before enabling Strict-Transport-Security headers please read into
        # this topic first.
        # add_header Strict-Transport-Security "max-age=15768000; includeSubDomains; preload;";
        #
        # WARNING: Only add the preload option once you read about
        # the consequences in https://hstspreload.org/. This option
        # will add the domain to a hardcoded list that is shipped
        # in all major browsers and getting removed from this list
        # could take several months.
        add_header X-Content-Type-Options nosniff;
        add_header X-XSS-Protection "1; mode=block";
        add_header X-Robots-Tag none;
        add_header X-Download-Options noopen;
        add_header X-Permitted-Cross-Domain-Policies none;
        # Optional: Don't log access to assets
        access_log off;
    }

    location ~ \.(?:png|html|ttf|ico|jpg|jpeg)$ {
        try_files $uri /index.php$request_uri;
        # Optional: Don't log access to other assets
        access_log off;
    }
}
```
3. 添加一個數據庫

**如果對console操作不是很習慣的話，也可以下載安裝一個'phpmyadmin'**

對數據庫進行初始設置

```bash
mysql_secure_installation
```
回答以下問題

```bash

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

You already have a root password set, so you can safely answer 'n'.

Change the root password? [Y/n] n
 ... skipping.

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] Y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] Y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] Y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] Y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

完成之後登入DB

```bash
mysql -u root -p
# 輸入password
```

創建一個nextcloud用的數據庫

```bash
CREATE DATABASE nextclouddb;
```

創建一個用戶

```bash
CREATE USER 'nextcloud'@'localhost' IDENTIFIED BY 'mypassword';
# 將'mypassword'換為自己的密碼
```

賦予用戶管理數據庫的權限

```bash
GRANT ALL PRIVILEGES ON nextclouddb.* TO 'nextcloud'@'localhost';
```

刷新數據庫

```bash
FLUSH PRIVILEGES
```

登出數據庫console

```bash
exit；
```

4. 安裝nextcloud

安裝所需軟件

```bash
apt-get install wget unzip -y
```

下載最新版

```bash
cd /tmp
wget https://download.nextcloud.com/server/releases/latest.zip
unzip latest.zip
```

將解壓後的文件夾移動到nginx的目錄下

```bash
mv nextcloud /home/wwwroot/your.domain/ # lnmp一鍵安裝的目錄
mv nextcloud /var/www/html/ # apt安裝的目錄
```

然後將前文nginx的conf文件中root一項改為nextcloud的目錄地址

```bash
root	/home/wwwroot/your.domain/nextcloud
# or
root	/var/www/html/nextcloud
```

1. 挂载硬盘

**将硬盘放入硬盤盒，並插在樹莓派的USB接口上。**

查看硬盤信息

```bash
# 我的硬盤識別為／dev/sda
fdisk -l | grep /dev/sda

/dev/sda1      34     262177     262144  128M Microsoft reserved
/dev/sda2  264192 7814036063 7813771872  3.7T Microsoft basic data
```

將硬盤格式化

```bash
 fdisk /dev/sda

 # partition刪除（指令d）
Command (m for help): d
Selected partition 1
Partition 1 has been deleted.

# partition新建（指令n）
Command (m for help): n
Partition number (1-128, default 1):
First sector (34-225112380, default 2048):
Last sector, +sectors or +size{K,M,G,T,P} (2048-225112380, default 225112380):

Created a new partition 1 of type 'Linux filesystem' and of size 107.3GiB.

# 確認硬盤信息（指令p）
Command (m for help): p
Disk /dev/sda: 107.4 GiB, 115257538560 bytes, 225112380 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xc3ffc3ff

Device     Boot Start       End   Sectors   Size Id Type
/dev/sda1        2048 225110015 225107968 107.3G  7 HPFS/NTFS/exFAT


# 保存操作（指令w）
Command (m for help): w
The partition table has been altered!
Calling ioctl() to re-read partition table.
Syncing disks.
```

將硬盤格式化為ex4（記好UUID）

```bash
mkfs.ext4 /dev/sda1

Creating filesystem with 976754385 4k blocks and 244195328 inodes
Filesystem UUID: fa16b72b-a2d0-4331-9693-e417727f3b53
Superblock backups stored on blocks:
    32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208,
    4096000, 7962624, 11239424, 20480000, 23887872, 71663616, 78675968,
    102400000, 214990848, 512000000, 550731776, 644972544

Allocating group tables: done
Writing inode tables: done
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done
```

開機自動掛載硬盤，打開／etc／fstab

```bash
nano ／etc/fstab
```

添加一行自動掛載並修改掛載文件夾的權限

```bash
uuid=fa16b72b-a2d0-4331-9693-e417727f3b53 ／{nextcloud.path}／data ntfs-3g quiet,locale=en_US.utf8,uid=1003,gid=1003,dmode=770,fmode=660,comment=systemd.automount 0 0
```

其中uid和gid可能需要修改為nginx user的數值

```bash
cat /etc/passwd
# 查找www或wwwdata的uid和gid
```

重啟

```bash
reboot
```

然後連接樹莓派的web，根據指示安裝並設置管理員。

**如果提示錯誤，請根據指示更改權限等。**

以上
