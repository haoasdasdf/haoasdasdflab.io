---
title: 為hexo增加comment功能-livere
tags:
  - hexo
  - livere
  - plugin
  - comment
date: 2018-10-25 14:00:52
---

## 前言

> 搭建了hexo博客之後，感覺確實很輕鬆。除了不能隨時隨地發表文章外(插件可解決)，其他都挺好的。但只有一點就是沒有comment功能，於是我準備加一個comment插件。

### Livere的預覽圖

![livere-preview.png](using-livere/livere-preview.png)

<!-- more -->

## comment插件的選擇

搜索了一下hexo可用的comment插件，真的是五花八門。

- Livere: 支持多種登入方式，有中、英、韓三語界面，註冊也比較簡單
- Valine: 開源，但需要[LeanCloud](https://leancloud.cn/)，由於是.cn域名，所以推測將來肯定要實名或備案
- 暢言: 推測將來肯定要實名或備案
- 友言: 推測將來肯定要實名或備案
- Hypercomments: 據說不錯，但官網中沒有看到Free plan，故沒有嘗試
- Disqus: 好像已被GFW阻隔
- Gitment: 因為開源所以應該是當中最安全的了，但只支持Github的帳戶
- ~~多說~~: 應該已經關閉了

**綜上所述，此次選擇了Livere**

## 註冊Livere

1. 去[Livere](https://www.livere.com/)官網註冊一個帳戶
2. 選擇免費的City版，點擊安裝
3. 進入管理頁面->點擊代碼管理->選擇一般網站，並取得data-uid

![Get-livere-uid](using-livere/livere-install.png)

## 在Hexo中設置Livere

### 如果所使用的主題中原本就支持livere的話，則只需在`theme/_config.yml`中找到填寫livere data-uid的項目將之前取得的data-uid填入即可。

- 通常主題都會預設文章下面的comment為開啟，如需在自建page也中同樣開啟評論功能的話則需要在`{project}/_config.yml`中修改或加入

```bash: _config.yml
page:
  comments: true
```

### 如所使用的主題中沒有livere選項的話，則需要自己進行一些設置

- 在`{theme}/_config.yml`中添加一項livere的設置

```bash
livere-uid: {your data-uid}
```

- 找到所用主題中的comments文件

>根據所使用主題不同，文件格式可能為js, ejs, jade, pug等，本片以ejs為例：  
`{theme}/layout/_partial/comments.ejs`  
>當然管理comments的文件路徑也可能有所不同比如  
`{theme}/layout/_partial/post/comment.ejs`等  

將獲取是uid時的那段script給copy一下，根據主題不同在文件合適的位置添加
```ejs:comments.ejs
# 可以將data-uid的部分修改為從theme設置中讀取
# <div id="lv-container" data-id="city" data-uid=<%= theme.livere_uid %>>

<% if (theme.livere_uid && page.comments){ %>
{Livere install code}
<% } %>
```

### **大功告成！**