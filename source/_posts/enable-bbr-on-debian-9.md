---
title: 在Debian 9中開啟BBR加速
tags:
	- linux
	- Debian
	- BBR
date: 2018-11-29 15:23:57
---

>BBR（Bottleneck Bandwidth and RTT）是Google發布的一種新的TCP擁塞控制算法。 它從4.9開始被添加到Linux內核中。 BBR不是將數據包丟失視為擁塞信號，而是依次探測瓶頸帶寬和RTT。

# 實行環境

- Debian 9 Stretch

首先確認kernel版本是否支持BBR:

```bash
uname -r # kernel >= 4.9
```

然後使用`lsmod | grep bbr`,來檢測是否已經開啟bbr。如果沒有的話：

```bash
# 加載bbr模塊
modprobe tcp_bbr
echo "tcp_bbr" >> /etc/modules-load.d/modules.conf
```

並且執行以下命令來開啟：

```bash
echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
sysctl -p
```

最後來測試一下是否成功開啟：

```bash
sysctl net.ipv4.tcp_available_congestion_control
# net.ipv4.tcp_available_congestion_control = bbr cubic reno
```

```bash
sysctl net.ipv4.tcp_congestion_control
# net.ipv4.tcp_congestion_control = cubic bbr
```

再次使用`lsmod | grep bbr`看看已加載的模塊
